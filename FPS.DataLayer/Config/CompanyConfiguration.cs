﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace FPS.DataLayer.Config
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable(nameof(Company));
            builder.HasKey(t => t.Id);
           // builder.Property(x => x.Id).ValueGeneratedNever();
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
        }
    }

   
}
