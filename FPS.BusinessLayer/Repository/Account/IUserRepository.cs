﻿using FPS.BusinessLayer.DTOs;
using FPS.BusinessLayer.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FPS.BusinessLayer.DTOs.ServiceRespones;

namespace FPS.BusinessLayer.Repository.Account
{
    public interface IUserRepository
    {
        Task<GeneralResponse> CreateAccount(UserSignUp userSignUpModel);

        Task<LoginResponse> LoginAccount(UserSignIn userSignInModel);

        Task<GeneralResponse> AddAccountByAdmin(UserAddByAdmin userAddByAdmin);

        Task<IEnumerable<UserInfo>> GetAllAccounts();

        Task<UserInfo> GetAccountById(string userId);
        Task<GeneralResponse> UpdateUser(string userId, UserInfo userInfo);

        Task<GeneralResponse> DeleteUser(string userId);

        Task<GeneralResponse> ForgotPassword(string email);



    }
}
