﻿using FPS.BusinessLayer.Service.EmailService;
using FPS.BusinessLayer.ViewModel.Account;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.Email;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static FPS.BusinessLayer.DTOs.ServiceRespones;

namespace FPS.BusinessLayer.Repository.Account
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _config;
        private readonly IEmailService _emailService;


        public UserRepository(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration config, IEmailService emailService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _config = config;
            _emailService = emailService;
        }

       

        public async Task<GeneralResponse> CreateAccount(UserSignUp userSignUpModel)
        {
            if (userSignUpModel is null) return new GeneralResponse(false, "Model is empty");
            var newUser = new User()
            {
                FullName = userSignUpModel.Name,
                Email = userSignUpModel.Email,
                UserName = userSignUpModel.Email,
                IsDeleted = false,
            };
            var user = await _userManager.FindByEmailAsync(userSignUpModel.Email);
            if (user != null) return new GeneralResponse(false, "User already registered");

            var createUser = await _userManager.CreateAsync(newUser, userSignUpModel.Password);
            if (!createUser.Succeeded) return new GeneralResponse(false, "Error during account creation");

            // Assign default role: admin to first register, rest are users
            var checkAdminRole = await _roleManager.FindByNameAsync("Admin");
            if (checkAdminRole == null)
            {
                await _roleManager.CreateAsync(new IdentityRole() { Name = "Admin" });
                await _userManager.AddToRoleAsync(newUser, "Admin");
            }
            else
            {
                var checkUserRole = await _roleManager.FindByNameAsync("User");
                if (checkUserRole == null)
                {
                    await _roleManager.CreateAsync(new IdentityRole() { Name = "User" });
                }
                await _userManager.AddToRoleAsync(newUser, "User");
            }

            return new GeneralResponse(true, "Account created successfully");
        }

        public async Task<LoginResponse> LoginAccount(UserSignIn userSignInModel)
        {
            if (userSignInModel == null)
                return new LoginResponse(false, null, "Login container is empty");

            var user = await _userManager.FindByEmailAsync(userSignInModel.Email);
            if (user == null)
            {
                return new LoginResponse(false, null, "User not found");
            }

            var validPassword = await _userManager.CheckPasswordAsync(user, userSignInModel.Password);
            if (!validPassword)
                return new LoginResponse(false, null, "Invalid email/password");

            var userRoles = await _userManager.GetRolesAsync(user);
            var userSession = new UserSession(user.Id, user.FullName, user.Email, userRoles.First());
            var token = GenerateToken(userSession);

            return new LoginResponse(true, token, "Login successful");
        }

        private string GenerateToken(UserSession user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var userClaims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role)
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: userClaims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public async Task<GeneralResponse> AddAccountByAdmin(UserAddByAdmin userAddByAdmin)
        {
            if (userAddByAdmin is null)
            {
                return new GeneralResponse(false, "Model is empty");
            }

            // Sinh mật khẩu ngẫu nhiên
            var newPassword = GenerateRandomPassword();

            var newUser = new User()
            {
                FullName = userAddByAdmin.Name,
                Email = userAddByAdmin.Email,
                UserName = userAddByAdmin.Email,
            };

            // Kiểm tra xem người dùng đã được đăng ký trước đó chưa
            var user = await _userManager.FindByEmailAsync(userAddByAdmin.Email);
            if (user != null) return new GeneralResponse(false, "User already registered");

            // Tạo người dùng mới
            var createUser = await _userManager.CreateAsync(newUser, newPassword);
            if (!createUser.Succeeded) return new GeneralResponse(false, "Error during account creation");

            // Gán vai trò cho người dùng mới
            if (!string.IsNullOrEmpty(userAddByAdmin.Role))
            {
                var roleExists = await _roleManager.RoleExistsAsync(userAddByAdmin.Role);
                if (!roleExists)
                {
                    return new GeneralResponse(false, $"Role '{userAddByAdmin.Role}' does not exist.");
                }

                await _userManager.AddToRoleAsync(newUser, userAddByAdmin.Role);
            }

            return new GeneralResponse(true, "Account created successfully");
        }

        private string GenerateRandomPassword()
        {
            // Chiều dài của mật khẩu
            int length = 12;

            // Kí tự cho phép trong mật khẩu
            string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()";

            // Sử dụng StringBuilder để tạo mật khẩu
            StringBuilder sb = new StringBuilder();
            Random random = new Random();

            // Sinh mật khẩu ngẫu nhiên bằng cách chọn ngẫu nhiên các kí tự từ validChars
            for (int i = 0; i < length; i++)
            {
                int index = random.Next(validChars.Length);
                sb.Append(validChars[index]);
            }

            return sb.ToString();
        }


        public async Task<IEnumerable<UserInfo>> GetAllAccounts()
        {
            var users = await _userManager.Users
                .Where(u => u.IsDeleted == false)
                .ToListAsync();

            var userInfos = new List<UserInfo>();
            foreach (var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);
                userInfos.Add(new UserInfo
                {
                    Id = Guid.Parse(user.Id),
                    Name = user.FullName,
                    Email = user.Email,
                    Role = roles.FirstOrDefault(),
                    DOB = user.DOB,
                    Address = user.Address,
                });
            }
            return userInfos;
        }

        public async Task<UserInfo> GetAccountById(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                // Trả về null nếu không tìm thấy người dùng với ID cung cấp
                return null;
            }

            // Lấy vai trò của người dùng
            var roles = await _userManager.GetRolesAsync(user);

            // Tạo đối tượng UserInfo từ thông tin người dùng và vai trò
            var userInfo = new UserInfo
            {
                Id = Guid.Parse(user.Id),
                Name = user.FullName,
                Email = user.Email,
                Role = roles.FirstOrDefault(),
                DOB = user.DOB,
                Address = user.Address,
                Status = user.Status // Cần đảm bảo rằng user.Status có sẵn trong User model
            };

            return userInfo;
        }



        public async Task<GeneralResponse> DeleteUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return new GeneralResponse(false, "User not found");
            }

            user.IsDeleted = true;
            await _userManager.UpdateAsync(user);

            return new GeneralResponse(true, "User soft deleted successfully");

        }

        public async Task<GeneralResponse> UpdateUser(string userId, UserInfo userInfo)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return new GeneralResponse(false, "User not found");
            }

            // Cập nhật thông tin người dùng
            user.FullName = userInfo.Name;
            user.Email = userInfo.Email;
            user.Address = userInfo.Address;
            user.Status = userInfo.Status;

            // Cập nhật người dùng trong cơ sở dữ liệu
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return new GeneralResponse(false, "Failed to update user");
            }

            return new GeneralResponse(true, "User updated successfully");

        }

        public async Task<GeneralResponse> ForgotPassword(string email)
        {
            // Tìm người dùng với địa chỉ email được cung cấp
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                // Người dùng không tồn tại
                return new GeneralResponse(false, "User not found");
            }

            // Sinh mật khẩu ngẫu nhiên mới
            string newPassword = GenerateRandomPassword();

            //gửi mail
        

            // Đặt mật khẩu mới cho người dùng
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);
            if (!result.Succeeded)
            {
                // Đặt lại mật khẩu thất bại
                return new GeneralResponse(false, "Error resetting password");
            }

            // Gửi mật khẩu mới cho người dùng (qua email hoặc các kênh khác)
            var emailDto = new EmailDTO
            {
                To = user.Email,
                Subject = "Your New Password",
                Body = $"<p>Your new password is: <strong>{newPassword}</strong></p><p>Please change it after logging in for security reasons.</p>"
            };

            // Send the new password via email
            _emailService.SendEmail(emailDto);
            return new GeneralResponse(true, "New password has been sent to the user");
        }


    }
}
