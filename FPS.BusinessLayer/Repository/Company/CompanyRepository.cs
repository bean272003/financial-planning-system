﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository,  ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }
        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName  = model.CompanyName;
            company.CreatedDate = DateTime.Now;
            company.UpdatedDate = DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }
        public List<CompanyViewModel> GetAllCompany()
        {
            return _context.Companies.Where(c => c.IsDeleted == false) 
                           .Select(c => new CompanyViewModel
                           {
                               CompanyName = c.CompanyName
                           }).
                           ToList();
        }
        public CompanyViewModel GetCompanyByID(Guid companyID)
        {
            var company = _context.Companies.FirstOrDefault(c => c.Id == companyID && c.IsDeleted == false);
            if (company != null)
            {
                var companyViewModel = new CompanyViewModel
                {
                    CompanyName = company.CompanyName
                };
                return companyViewModel;
            }
            return null; 
        }
        public void UpdateCompany(Guid companyID, CompanyModel model)
        {
            var companyToUpdate = _context.Companies.FirstOrDefault(c => c.Id == companyID);
            if (companyToUpdate != null)
            {
                companyToUpdate.CompanyName = model.CompanyName;
                companyToUpdate.UpdatedDate = DateTime.Now;
                companyToUpdate.UpdatedBy = model.UpdatedBy;
                _context.SaveChanges();
            }
        }
        public void DeleteCompany(Guid companyID)
        {
            var companyToDelete = _context.Companies.FirstOrDefault(c => c.Id == companyID);
            if (companyToDelete != null)
            {
                companyToDelete.IsDeleted = true; 
                _context.SaveChanges();
            }
        }

    }
}
