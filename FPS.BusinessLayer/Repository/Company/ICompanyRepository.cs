﻿using FPS.BusinessLayer.ViewModel.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);

        public List<CompanyViewModel> GetAllCompany();

        public CompanyViewModel GetCompanyByID(Guid companyID);

        public void UpdateCompany(Guid companyID, CompanyModel model);

        public void DeleteCompany(Guid companyID);
        

    }
}
