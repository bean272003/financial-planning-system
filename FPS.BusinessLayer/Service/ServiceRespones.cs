﻿using FPS.BusinessLayer.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.DTOs
{
    public class ServiceRespones
    {
        public record class GeneralResponse(bool Flag, string Message);

        public record class LoginResponse(bool Flag, string Token, string Message);

        public record class GetAllAccountsResponse(bool Flag, string Message, Object Data);
    }
}
