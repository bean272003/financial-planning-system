﻿using FPS.BusinessLayer.ViewModel.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Service.EmailService
{
    public interface IEmailService
    {
        void SendEmail(EmailDTO request);
    }
}
