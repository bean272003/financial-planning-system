﻿using FPS.BusinessLayer.Repository.Account;
using FPS.BusinessLayer.Service.EmailService;
using FPS.BusinessLayer.ViewModel.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        private readonly IUserRepository _userRes;
        private readonly IEmailService _emailService;

        public AccountController(IUserRepository userRes, IEmailService emailService)
        {
            _userRes = userRes;
            _emailService = emailService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserSignUp userSignUp)
        {
            var response = await _userRes.CreateAccount(userSignUp);
            return Ok(response);
        }

        [HttpPost("login")]

        public async Task<IActionResult> Login(UserSignIn userSignIn)
        {
            var response = await _userRes.LoginAccount(userSignIn);
            if (response.Flag == true)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return BadRequest("Email is required");
            }

            var response = await _userRes.ForgotPassword(email);
            if (response.Flag == true)
            {
                return Ok(response.Message);

            }
            else
            {
                return BadRequest();

            }
        }

    }
}
