﻿using FPS.BusinessLayer.Service.EmailService;
using FPS.BusinessLayer.ViewModel.Email;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Templates.BlazorIdentity.Pages.Manage;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendMail : ControllerBase
    {

        private readonly IEmailService _emailService;

        public SendMail(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost]
        public IActionResult SendEmail(EmailDTO request)
        {
            _emailService.SendEmail(request);
            return Ok();
        }
    }
}
