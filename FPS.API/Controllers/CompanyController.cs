﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Route("AddCompany")]

        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpGet]
        [Route("GetAllCompany")]
        [Authorize(Roles = "Admin")]
        public ActionResult GetAllCompany()
        {
            var companies = _companyRepository.GetAllCompany();
            return Ok(companies);
        }


        [HttpGet]
        [Route("GetCompanyById/{companyId}")]
        public ActionResult GetCompanyById(Guid companyId)
        {
            try
            {
                var company = _companyRepository.GetCompanyByID(companyId);
                if (company == null)
                {
                    return NotFound($"Company with ID {companyId} not found.");
                }
                return Ok(company);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }



        [HttpPut]
        [Route("UpdateCompany/{companyId}")]
        public IActionResult UpdateCompany(Guid companyId, CompanyModel model)
        {
            try
            {
                _companyRepository.UpdateCompany(companyId, model);
                return Ok("Company updated successfully.");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("DeleteCompany/{companyId}")]
        public IActionResult DeleteCompany(Guid companyId)
        {
            try
            {
                _companyRepository.DeleteCompany(companyId);
                return Ok("Company deleted successfully.");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
   