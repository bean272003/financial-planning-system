﻿using FPS.BusinessLayer.Repository.Account;
using FPS.BusinessLayer.ViewModel.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AdminController : ControllerBase
    {
        private readonly IUserRepository _userRes;
        public AdminController(IUserRepository userRes)
        {
            _userRes = userRes;
        }

        [HttpPost("addUser")]
        public async Task<IActionResult> AddUserByAdmin(UserAddByAdmin userAddByAdmin)
        {
            var response = await _userRes.AddAccountByAdmin(userAddByAdmin);
            return Ok(response);
        }
        [HttpGet("getAllAccounts")]

        public async Task<IActionResult> GetAllAccounts()
        {
            var accounts = await _userRes.GetAllAccounts();
            return Ok(accounts);
        }

        [HttpGet("getAccountById/{userId}")]
        public async Task<IActionResult> GetAccountById(string userId)
        {
            var account = await _userRes.GetAccountById(userId);
            if (account == null)
            {
                return NotFound("User not found");
            }
            return Ok(account);
        }


        [HttpPut("updateUser/{userId}")]
        public async Task<IActionResult> UpdateUser(string userId, UserInfo userInfo)
        {
            var response = await _userRes.UpdateUser(userId, userInfo);
            return Ok(response);
        }

        [HttpDelete("deleteUser/{userId}")]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var response = await _userRes.DeleteUser(userId);
            return Ok(response);
        }

    }
}
