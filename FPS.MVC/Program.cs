using FPS.MVC.Service.Admin;
using FPS.MVC.Service.Authentication;
using FPS.MVC.Service.Company;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddHttpClient<ICompanyService, CompanyService > (c => c.BaseAddress
    = new Uri("http://localhost:5235/"));
builder.Services.AddHttpClient<IUserService, UserService>(c => c.BaseAddress
    = new Uri("http://localhost:5235/"));
builder.Services.AddHttpClient<IAdminService, AdminService>(c => c.BaseAddress
    = new Uri("http://localhost:5235/"));



//config session
builder.Services.AddHttpContextAccessor();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(10);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});






var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Account}/{action=Login}/{id?}");

app.Run();
