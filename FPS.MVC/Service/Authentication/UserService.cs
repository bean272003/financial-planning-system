﻿
using FPS.BusinessLayer.DTOs;
using FPS.BusinessLayer.Repository.Account;
using FPS.BusinessLayer.ViewModel.Account;
using FPS.MVC.APIConfig;
using FPS.MVC.Models;
using Microsoft.AspNetCore.Identity.Data;
using Newtonsoft.Json;
using NuGet.Common;
using System.Net.Http;
using System.Text;
using static FPS.BusinessLayer.DTOs.ServiceRespones;

namespace FPS.MVC.Service.Authentication
{
    public class UserService : BaseService, IUserService
    {
        public UserService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
        }

        public async Task<LoginResponse> LoginAccount(UserSignIn userSignIn)
        {
            var loginData = new { userSignIn.Email, userSignIn.Password };
            var json = JsonConvert.SerializeObject(loginData);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.LOGIN_ACCOUNT, content);

            if (response.IsSuccessStatusCode)
            {
                var tokenResponse = await response.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<TokenResponse>(tokenResponse);
                var userLogged = new LoginResponse(true, token.Token.ToString(), "Login successfully");
                return userLogged;
            }

            else
            {
                var user = new LoginResponse(false, null, "Login failed");
                return user;
            }
        }


        public async Task<GeneralResponse> ForgotPassword(string email)
        {
            var jsonRequest = JsonConvert.SerializeObject(new { email = email });
            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync($"{PathConfig.FORGOT_PASSWORD}?email={email}", content);
            if (response.IsSuccessStatusCode)
            {
                return new GeneralResponse(true, "Successful to reset password");
            }
            return new GeneralResponse(false, "Failed to reset password");
        }
    }
}
