﻿using FPS.BusinessLayer.ViewModel.Account;
using FPS.MVC.Models;
using Microsoft.AspNetCore.Identity.Data;
using Org.BouncyCastle.Bcpg.OpenPgp;
using static FPS.BusinessLayer.DTOs.ServiceRespones;

namespace FPS.MVC.Service.Authentication
{
    public interface IUserService
    {
        Task<LoginResponse> LoginAccount(UserSignIn userSignIn);


        Task<GeneralResponse> ForgotPassword(string email);
    }
}
