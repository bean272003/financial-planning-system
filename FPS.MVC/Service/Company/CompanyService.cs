﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.MVC.APIConfig;

namespace FPS.MVC.Service.Company
{
    public class CompanyService : BaseService, ICompanyService
    {
        public CompanyService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
        }

        public List<CompanyViewModel> GetAllCompany()
        {
            var response = _client.GetAsync(PathConfig.GET_ALL_COMPANY);
            return null;
        }  
    } 
}
