﻿using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Service.Company
{
    public interface ICompanyService
    {
        public List<CompanyViewModel> GetAllCompany();
    }
}
