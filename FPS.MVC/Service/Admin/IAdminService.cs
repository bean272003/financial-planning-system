﻿using FPS.BusinessLayer.ViewModel.Account;
using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Service.Admin
{
    public interface IAdminService
    {

        Task<List<UserInfo>> GetAllAccount();
        Task<UserInfo> GetUserInfoById(string userId);
        Task<bool> UpdateAccount(string userId, UserInfo updatedUserInfo);
    }
}
