﻿
using FPS.BusinessLayer.ViewModel.Account;
using FPS.MVC.APIConfig;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace FPS.MVC.Service.Admin
{
    public class AdminService : BaseService, IAdminService
    {
        public AdminService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
            var accessToken = httpContextAccessor.HttpContext.Session.GetString("AccessToken");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }
     
        public async Task<List<UserInfo>> GetAllAccount()
        {
            try
            {
                var response = await _client.GetAsync(PathConfig.GET_ALL_ACCOUNT);
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var accounts = JsonConvert.DeserializeObject<List<UserInfo>>(jsonString);
                    return accounts;
                }
                else
                {
                    return new List<UserInfo>();
                }
            }
            catch (Exception ex)
            {

                return new List<UserInfo>();
            }
        }

        public async Task<UserInfo> GetUserInfoById(string userId)
        {
            try
            {
                // Gọi API để lấy thông tin người dùng theo ID
                var response = await _client.GetAsync($"{PathConfig.GET_USER_BY_ID}/{userId}");

                if (response.IsSuccessStatusCode)
                {
                    // Đọc dữ liệu trả về từ API
                    var jsonString = await response.Content.ReadAsStringAsync();
                    // Chuyển đổi dữ liệu thành đối tượng UserInfo
                    var userInfo = JsonConvert.DeserializeObject<UserInfo>(jsonString);
                    return userInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                // Xử lý ngoại lệ khi gọi API
                // Ví dụ: Log lỗi và trả về giá trị mặc định
                return null;
            }
        }

        public async Task<bool> UpdateAccount(string userId, UserInfo updatedUserInfo)
        {
            try
            {
                var data = new { updatedUserInfo.Name, updatedUserInfo.Email,updatedUserInfo.Address,updatedUserInfo.Status, updatedUserInfo.Role };
                var serializedData = JsonConvert.SerializeObject(data);
                var content = new StringContent(serializedData, Encoding.UTF8, "application/json");
                var response = await _client.PutAsync($"{PathConfig.UPDATE_ACCOUNT}/{userId}", content);

                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                // Xử lý lỗi
                return false;
            }
        }
       
    }
}
