﻿namespace FPS.MVC.APIConfig
{
    public class PathConfig
    {
        public const string GET_ALL_COMPANY = "/api/Company/GetAllCompany";
        public const string GET_ALL_ACCOUNT = "/api/Admin/getAllAccounts";
        public const string UPDATE_ACCOUNT = "/api/Admin/updateUser";
        public const string GET_USER_BY_ID = "/api/Admin/getAccountById";
        public const string LOGIN_ACCOUNT = "/api/Account/login";
        public const string FORGOT_PASSWORD = "/api/Account/ForgotPassword";




    }
}
