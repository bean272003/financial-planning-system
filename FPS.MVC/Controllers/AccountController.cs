﻿using FPS.BusinessLayer.ViewModel.Account;
using FPS.MVC.Service.Authentication;
using Microsoft.AspNetCore.Identity.Data;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(string email, string password)
        {
            var signIn = new UserSignIn();
            signIn.Email = email;
            signIn.Password = password;
            // Gọi service để thực hiện đăng nhập
            var loginResponse = await _userService.LoginAccount(signIn);

            if (loginResponse.Flag)
            {
                // Đăng nhập thành công, chuyển hướng đến trang chính của ứng dụng
                HttpContext.Session.SetString("AccessToken", loginResponse.Token);
                HttpContext.Session.SetString("AccountLogin", email);

                // Nếu đăng nhập thành công, chuyển hướng đến trang chính
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Nếu đăng nhập thất bại, hiển thị thông báo lỗi
                ModelState.AddModelError("login-failed", "Email or Password Incorrect!");
                return View();
            }
        }


        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Account");
        }


        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            var response = await _userService.ForgotPassword(email);
            if (response.Flag)
            {
                return RedirectToAction("Login", "Account");
            } else
            {
                ModelState.AddModelError("login-failed", "Not Found Email");
                return View();
            }
        }

    }
}
