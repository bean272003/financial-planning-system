﻿using FPS.BusinessLayer.ViewModel.Account;
using FPS.MVC.Service.Admin;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class AdminController : Controller
    {

        public readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAccount()
        {
            var accounts = await _adminService.GetAllAccount();
            return View(accounts);
        }
        public async Task<IActionResult> UpdateAccount(string userId)
        {
            var userInfo = await _adminService.GetUserInfoById(userId); 
            return View(userInfo);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateAccount(string userId, UserInfo userInfo)
        {
            var result = await _adminService.UpdateAccount(userId, userInfo);

            if (result)
            {
                return RedirectToAction("GetAllAccount");
            }
            else
            {
                // Nếu cập nhật không thành công, hiển thị trang cập nhật lại với thông tin người dùng đã nhập
                return View("UpdateAccount", userInfo);
            }
        }
    }
}
